import controller_template as controller_template
from random import randint
import numpy as np
from array import array 
from .geracao import Geracao

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)



    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################

    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
        features = self.compute_features(self.sensors)
        right_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[:3] ])
        left_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[3:6] ])
        acc_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[6:9] ])
        brake_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[9:12] ])
        nothing_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[12:15] ])
        max_value = max([right_score, left_score, acc_score, brake_score, nothing_score])
        if max_value == right_score:
            return 1
        if max_value == left_score:
            return 2
        if max_value == acc_score:
            return 3
        if max_value == brake_score:
            return 4
        if max_value == nothing_score:
            return 5


    def compute_features(self, sensors):
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                      
            track_distance_center: 1-100                    
            track_distance_right: 1-100                     
            on_track: 0 or 1                                
            checkpoint_distance: 0-???                      
            car_velocity: 10-200                            
            enemy_distance: -1 or 0-???                     
            position_angle: -180 to 180                     
            enemy_detected: 0 or 1  
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """
        f1 = sensors[1]
        f2 = sensors[0] - sensors[2]
        f3 = sensors[5] / 2
        return [f1, f2, f3] * 5




    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE
        
        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
       """
        
        # Criação da primeira geração do algoritmo
        umaGeracao = Geracao()
        for y in range(0,9):
            umaGeracao.listaNodos[y].valores = [randint(0,1000) / 4000 for x in range(15)]
        for y in range(0,9):
            umaGeracao.listaNodos[y].score = self.run_episode(umaGeracao.listaNodos[y].valores)

        umaGeracao.listaNodos[0].valores = weights[:15].copy()
        umaGeracao.listaNodos[0].score = self.run_episode(umaGeracao.listaNodos[0].valores)


        listaAux = [umaGeracao.listaNodos[x].score for x in range(0,9)]
        print(listaAux)
        best_score = max(listaAux)
        for y in range(0,9):
            if (umaGeracao.listaNodos[y].score == best_score):
                best = umaGeracao.listaNodos[y].valores


        candidate = []
        candidate_score = 0
        a=np.arange(10000)
        cont = 0
        while True:
            try:
                
                cont = cont +1


                novaGeracao = umaGeracao.darwinismo(self,0) 

                listaAux = [novaGeracao.listaNodos[x].score for x in range(0,9)]
                #print("lista aux:",listaAux)
                candidate_score = max(listaAux)

                for y in range(0,9):
                    if (novaGeracao.listaNodos[y].score == candidate_score):
                        best = novaGeracao.listaNodos[y].valores
                print("Score: ",candidate_score)

                if(candidate_score > best_score): # novo best scorte foi atingido
                    print ("\n\n\nNovo Best Score!:")
                    print ("---------------------------------")
                    print (candidate_score)
                    print (candidate)
                    print ("---------------------------------")
                    best = candidate.copy()
                    best_score = candidate_score


            except KeyboardInterrupt:
                break
        print("\n\n\nAlgoritmo genetico finalizado após ",cont," geracoes!")    
        print("best score: ",best_score)
        print ("best params: ",best)
        
        return best