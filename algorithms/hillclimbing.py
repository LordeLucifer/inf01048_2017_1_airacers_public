from random import randint
import matplotlib.pyplot as plt
import numpy as np



def hillclimbing(controller, weights, ft):

    max_iterations = 2

    Y = []
    X = []

    j = 0
    current_state = weights[:ft].copy()
    current_score = controller.run_episode(current_state)

    print(current_score)
    for i in range(max_iterations):
        candidates = generate_neighbors(current_state)
        best_neighbor = current_state.copy()
        best_neighbor_score = current_score
        for candidate in candidates:
            candidate_score = controller.run_episode(candidate)
            if candidate_score > best_neighbor_score:
                X.append(int(i + j))
                Y.append(best_neighbor_score)
                print(candidate_score)
                best_neighbor_score = candidate_score
                best_neighbor = candidate.copy()
                j += 1

        if best_neighbor_score >= current_score:
            current_score = best_neighbor_score
            current_state = best_neighbor.copy()
        else:
            print("Found local maximum")
            print(current_score)
            break
    plt.plot(np.array(X), np.array(Y))
    plt.show()
    return current_state


def generate_neighbors(current_state):
    neighbors = []

    max_iterations = 10
    perturbation_range = 250

    for i in range(max_iterations):
        to_append = current_state.copy()
        for change_feature in range(20):
            position = randint(0, len(current_state) -1)
            perturbation = randint(-perturbation_range,perturbation_range)
            to_append[position] += perturbation
        neighbors.append(to_append)
    return neighbors

