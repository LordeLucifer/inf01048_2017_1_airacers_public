from random import randint
import matplotlib.pyplot as plt
import numpy as np

EULER_NUM = 2.71828


def sim_annealing(controller, weights, ft):

    max_iterations = 150
    temperature = 200

    X = []
    Y = []
    current_state = weights[:ft].copy()
    current_score = controller.run_episode(current_state)

    print(current_score)
    iter = 1
    for i in range(max_iterations):
        iter += 1
        temperature -= 1
        if temperature <= 0:
            Y.append(current_score)
            X.append(iter)
            iter += 1
            print(current_score)

            plt.plot(np.array(X), np.array(Y))
            plt.show()
            return current_state

        for j in range(5):
            candidate = generate_neighbor(current_state)
            candidate_score = controller.run_episode(candidate)
            delta_e = candidate_score - current_score
            if delta_e > 0:
                if int(candidate_score) != int(current_score):
                    X.append(iter)
                    Y.append(candidate_score)
                    print(current_score)
                    iter += 1
                current_state = candidate
                current_score = candidate_score
            else:
                probability = EULER_NUM ** (delta_e / temperature)
                array = [0] + [1] * (int(100 * probability))
                index = randint(0, len(array) - 1)
                if array[index] == 1:
                    candidate = get_random_candidate(current_state)
                    candidate_score = controller.run_episode(candidate)
                    # print("Moving to other point")
                    if int(candidate_score) != int(current_score):
                        print(current_score)
                        Y.append(candidate_score)
                        X.append(iter)
                        iter += 1
                    current_state = candidate
                    current_score = candidate_score
    print(current_score)
    plt.plot(np.array(X), np.array(Y))
    plt.show()
    return current_state


def get_random_candidate(current_state):
    perturbation_range = 1000

    to_append = current_state.copy()
    for change_feature in range(10):
        to_append[change_feature] += randint(-perturbation_range, perturbation_range)
    return to_append

def generate_neighbor(current_state):
    perturbation_range = 1000

    to_append = current_state.copy()
    for change_feature in range(13):
        position = randint(0, len(current_state) -1)
        perturbation = randint(-perturbation_range, perturbation_range)
        to_append[position] += perturbation
    return to_append
