import random
import numpy as np

import matplotlib.pyplot as plt

def randomalg(controller, weights, ft):

    best = weights[:ft]
    best_score = controller.run_episode(best)


    X = []
    Y = []
    a = np.arange(100)

    for i in np.nditer(a):
        candidate = [(random.randint(0, 300) / 400) * 150 for i in range(0, ft)]
        candidate_score = controller.run_episode(candidate)
        X.append(int(i))
        Y.append(candidate_score)
        if candidate_score >= best_score:
            best = candidate.copy()
            best_score = candidate_score
            print(best_score)
    plt.plot(np.array(X), np.array(Y))
    plt.show()
    return best
