

from algorithms.hillclimbing import hillclimbing
from algorithms.random import random, randomalg


def mixture(controller, tetha):
    weights = tetha.copy()
    score = controller.run_episode(weights)

    while score < 8000:
        weights = randomalg(controller, weights, 15)
        score = controller.run_episode(weights)
        print(score)

    return hillclimbing(controller, weights, 15)