from random import randint

import numpy as np

import controller_template as controller_template

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)



    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################

    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
        features = self.compute_features(self.sensors)
        right_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[:3] ])
        left_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[3:6] ])
        acc_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[6:9] ])
        brake_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[9:12] ])
        nothing_score = sum([weight*feature for weight, feature in list(zip(parameters, features))[12:15] ])
        max_value = max([right_score, left_score, acc_score, brake_score, nothing_score])
        if max_value == right_score:
            return 1
        if max_value == left_score:
            return 2
        if max_value == acc_score:
            return 3
        if max_value == brake_score:
            return 4
        if max_value == nothing_score:
            return 5


    def compute_features(self, sensors):
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                    0   
            track_distance_center: 1-100                   1 
            track_distance_right: 1-100                     2
            on_track: 0 or 1                                3
            checkpoint_distance: 0-???                      4
            car_velocity: 10-200                            5
            enemy_distance: -1 or 0-???                     6
            position_angle: -180 to 180                     7
            enemy_detected: 0 or 1                          8
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """
        f1 = sensors[1]
        f2 = sensors[0] - sensors[2]
        f3 = sensors[5] / 1.2
        return [f1, f2, f3] * 5




    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE
        
        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        return None
